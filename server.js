const express = require('express');
const app = express();
const server = require('http').Server(app)
const io = require('socket.io')(server);
/* const io = require('socket.io')(server, {
    cors: {
        origin: "http_//localhost:4200",
        methods: ["GET", "POST"]
    }
})
 */
const {v4 : uuidV4 } = require('uuid')

app.set('view engine', 'ejs')
//app.use(require('cors')())
app.use(express.static('public'))

app.get('/', (req, res) => {
    res.redirect(`/${uuidV4()}`)
})

app.get('/:room', (req, res) => {
    res.render('room', {roomId: req.params.room})
})

io.on('connection', socket =>{
    socket.on('join-room', (roomId, userId) => {
        console.log(roomId, userId);
        socket.join(roomId);
        socket.broadcast.to(roomId).emit('user-connected', userId)
        //socket.to(roomId).broadcast.emit('user-connected', userId);
    })
})

server.listen(3000)
