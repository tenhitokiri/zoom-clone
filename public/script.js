const socket = io('/')
const videoGrid = document.getElementById('video-grid')
const myPeer = new Peer(undefined, {
    host: '/',
    port: 3001
}) 

const myVideo = document.createElement('video');
myVideo.muted = true;

navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true
}).then(stream => {
    console.log("good");
    addVideoStream(myVideo, stream)
}).catch(function(err) { console.log(err.name + ": " + err.message); }); // always check for errors at the end.

/* .catch((err) => {
    console.log("error");
    console.log(err.name + ": " + err.message);
})
 */
myPeer.on('open', (id) => {
    socket.emit('join-room', ROOM_ID, id)
})

socket.on('user-connected', userId =>{
    console.log('User connected: ' + userId);
})

function addVideoStream(video, stream){
    video.srcObject = stream
    video.addEvenListener('loadedmetadata', () => {
        video.play()
    })
    videoGrid.append(video)
}